'use strict';
var moongoose = require('mongoose');
var Schema = moongoose.Schema;

var PurestListSchema = new Schema({
    title:{
        type: String,
        required: 'The purest title is required'
    },
    description:{
        type: String,
        required: 'The purest description is required'
    },
    image: {
        type: String
    },
    rating: {
        type: String
    },
    created_at: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('PurestList', PurestListSchema);