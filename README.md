# Purisima Api

This is an api for purest project

## Authors

* **Cristhian Aguirre** - *iOS Developer* - [Profile](https://github.com/CristhianUNI)
* **Luis Muñoz** - *Android Developer* - [Profile](https://github.com/LuisMMT)
* **René Cortez** - *Backend Developer* - [Profile](https://github.com/rjcd95)

## License

This project is licensed under the MIT License